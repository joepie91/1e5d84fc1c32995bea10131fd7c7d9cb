// file: serial.js

const SerialPort = require('serialport')

module.exports = function() {
  const port = SerialPort('path/to/serial/port')
  const e = new events.EventEmitter()

  // listen for incoming serial data
  port.on('data', function (data) {
    // emit event 'serialdata'
    e.emit('serialdata', data);
  })
          
  return e
}



// file app.js

const serial = require('./serial')
const events = require('events')

const e = serial()

e.on('serialdata', ()=>{
  console.log('Serial data received')
})
